create database books;

use books;

CREATE TABLE ursql (
TOPIC VARCHAR(20),
SUBTOPIC TEXT,
SYNTAX TEXT,
OPTIONS TEXT,
DESCRIPTION TEXT,
EXAMPLE TEXT,
OUTPUT TEXT
);

DESC ursql;
INSERT INTO ursql
VALUES ('DATABASE', '', '', '', '', '', '');

INSERT INTO ursql
VALUES ('', '', 'CREATE DATABASE database_name;', '','CREATE DATABASE database_name;', '', '');

INSERT INTO ursql
VALUES ('', '', 'SHOW DATABASES;', '', 'list available databases', '', '');


INSERT INTO ursql
VALUES ('', '', 'USE database_name;', '', 'use a database', '', '');

INSERT INTO ursql
VALUES ('', '', 'DROP DATABASE database_name;', '', 'delete a database', '', '');

INSERT INTO ursql
VALUES ('TABLE', '', '', '', '', '', '');

INSERT INTO ursql
VALUES ('', '', 'SHOW TABLES', '', 'list tables', '', '');

INSERT INTO ursql
VALUES ('', '', 'CREATE TABLE table_name(
column1 datatype(size),
column2 datatype(size) constraints, 
column3 datatype(size) constraint1 constraint2...);', '', 'create a table
create column with specific datatype
create column with specific datatype & constraint (described earlier)', '', '');

INSERT INTO ursql
VALUES ('', '', 'DESC table_name;', '', 'describe structure of table', '', '');

INSERT INTO ursql
VALUES ('', '', 'RENAME TABLE table_name TO new_name;', '', 'describe structure of table', '', '');

INSERT INTO ursql
VALUES ('', '', 'DROP TABLE table_names', '', 'delete tables', '', '');

INSERT INTO ursql
VALUES ('INSERT', '', '', '', '', '', '');

INSERT INTO ursql
VALUES ('', 'insert into all columns of a table', 'INSERT INTO table_name
VALUES (value1, value2, value3 ...);', '', '
the order of values must correspond to the order of
columns in the table', '', '');

INSERT INTO ursql
VALUES ('', 'insert into specified columns only', 'INSERT INTO table_name (column1, column2, column3, ...)
VALUES (value1, value2, value3 ...);', '', 'the order of values must correspond to the order of
columns in the table', '', '');

INSERT INTO ursql
VALUES ('', 'populate a table with another table', 'INSERT INTO first_table (column1, column2, ... columnN)
SELECT column1, column2, ...columnN
FROM second_table
WHERE (conditions);', '', '', '', '');

INSERT INTO ursql
VALUES ('QUERIES', '', '', '', '', '', '');

INSERT INTO ursql
VALUES ('', 'SELECT', '
SELECT  (table_name.column_name)', '

*
(table1.column_name AS name)
t2.column_name
MIN(table_name.column_name)
MAX(table_name.column_name)
SUM(table_name.column_name)
SQRT(table_name.column_name)
AVG(table_name.column_name)
UPPER(table_name.column_name)
LOWER(table_name.column_name)
DISTINCT(table_name.column_name)
COUNT(table_name.column_name)
DISTINCT COUNT(table_name.column_name)
', '
table names are specified in front of columns using .
select all columns
rename the column in output with AS
t2 is alias of table2
minimum value of the numeric column
maximum value of the numeric column
sum of all values in numeric column
square root value of numeric column
average of all values in numeric column
values of the column will be turned into uppercase
values of the column will be turned into lowercase
exclude duplicate values
count number of values
count number of unrepeated values
', '', '');

INSERT INTO ursql
VALUES ('', 'FROM', '
FROM (table_names)', '

FROM table1,
table2 AS t2', '
select tables to pull data from
one or more tables can be specified at once
set table alias with AS
', '', '');

INSERT INTO ursql
VALUES ('', 'LIMIT', 'LIMIT   <start>, <end>', '', 'show some limited rows from <start> to <end>', '', '');

INSERT INTO ursql
VALUES ('', 'WHERE', '
WHERE (conditions)', '

column IN (value1, value2 ...)
column NOT IN (value6, value7, ...)
column IS NULL
column IS NOT NULL
condition1 AND condition2
condition1 OR (cond2 AND cond3)
condition1 AND (condition2 OR condition3)
column_name operator (query)
column LIKE <pattern>
', '

include some specific values from the column
exclude some specific values from the column
include only NULL values from the column
exclude NULL values from the column
both conditions should be applied
one of the conditions should be applied
valid condition
valid condition
sub query
output values with the same pattern
where, "_" matches one arbitrary character &
"%" matches one or more arbitrary characters
', '', '');

INSERT INTO ursql
VALUES ('', 'ORDER BY', '
ORDER BY (column_names)', '

ORDER BY (column_names) ASC
ORDER BY (column_names) DESC', '

sort in ascending order
sort in descending order
', '', '');

INSERT INTO ursql
VALUES ('UPDATE', '', '', '', '', '', '');

INSERT INTO ursql
VALUES ('', '', 'UPDATE table_name
SET column1 = value1, column2 = value2....
WHERE (conditions);', '', 'modify records
assign new values to columns with SET
omition of where clause will update all rows
', '', '');

INSERT INTO ursql
VALUES ('DELETE', '', '', '', '', '', '');

INSERT INTO ursql
VALUES ('', '', 'DELETE FROM table_name
WHERE (conditions);', '', 'delete rows
omition of WHERE clause will delete all rows', 
'', '');

INSERT INTO ursql
VALUES ('TABLE JOIN', '', '', '', '', '', '');

INSERT INTO ursql
VALUES ('', '', 'SELECT
table1.column_a, table2.column_b, table3.column_a...
FROM table1, table2, table3...
WHERE table1.column_c=table2.column_d=table3.colum...
', '', '


mandatory conditions, there should be an intersecting
column on the basis of which tables will join',
'', '');

INSERT INTO ursql
VALUES ('', 'types of join', '', '', '', '', '');

INSERT INTO ursql
VALUES ('', '1.inner join', '
SELECT  table1.column_a, table2.column_b
FROM table1 INNER JOIN table2
ON table1.column_c=table2.column_c;', '', 'returns rows when there is a match
in both tables

ON specifies the join condition', '', '');

INSERT INTO ursql
VALUES ('', '1.left join', '
SELECT  table1.column_a, table2.column_b
FROM table1 LEFT JOIN table2
ON table1.column_c=table2.column_c;', '', 'returns all rows from the left table 
& matched rows from right table

ON specifies the join condition', '', '');

INSERT INTO ursql
VALUES ('', '1.right join', '
SELECT  table1.column_a, table2.column_b
FROM table1 RIGHT JOIN table2
ON table1.column_c=table2.column_c;', '', 'returns all rows from the right table
& matched rows from left table

ON specifies the join condition', '', '');

INSERT INTO ursql
VALUES ('', '1.cartesion join', '
SELECT  table1.column_a, table2.column_b
FROM table1 CARTESIAN JOIN table2
ON table1.column_c=table2.column_c;', '', 'returns the Cartesian product of the sets of
records from the two or more joined tables

ON specifies the join condition', '', '');

INSERT INTO ursql
VALUES ('UNION', '', '', '', '', '', '');

INSERT INTO ursql
VALUES ('', 'union', 'SELECT column_names FROM table1
UNION
SELECT column_names FROM table2', '', 'combine the results of two or more SELECT
statements without returning any duplicate rows', '', '');

INSERT INTO ursql
VALUES ('', 'union all', 'SELECT column_names FROM table1
UNION ALL
SELECT column_names FROM table2', '', 'combine the results of two SELECT 
statements including duplicate rows', '', '');

INSERT INTO ursql
VALUES ('ALTER', '', '', '', 'play with columns', '', '');

INSERT INTO ursql
VALUES ('', '', 'ALTER TABLE table_name ADD column_name;
ALTER TABLE table_name DROP column_name;
ALTER TABLE table_name RENAME column_name TO newname;
ALTER TABLE table_a MODIFY column_a datatype constraints;
ALTER TABLE table_name ADD PRIMARY KEY (columns..);
ALTER TABLE table_name DROP PRIMARY KEY (columns..);', '',
'add a New Column in an existing table
drop column in an existing table
rename column
change the DATA TYPE of a column
add PRIMARY KEY
drop PRIMARY KEY
', '', '');

INSERT INTO ursql
VALUES ('TRANSACTION', '', '', '', '', '', '');

INSERT INTO ursql
VALUES ('', '', 'COMMIT;
ROLLBACK;
SAVEPOINT savepoint_name;
ROLLBACK TO savepoint_name;
RELEASE SAVEPOINT savepoint_name;
SET AUTOCOMMIT = 0
SET AUTOCOMMIT = 1', '',
'save the changes
undo the changes since last commit
creates points within the groups of 
transactions to which we can ROLLBACK
undo a group of transaction
delete a savepoint
no activities are committed automatically
each SQL statement is considered as a complete
transaction and comitted', '', '');

INSERT INTO ursql
VALUES ('VIEW', '', '', '', 'a composition of a table in the form of 
a predefined SQL query', '', '');

INSERT INTO ursql
VALUES ('', '', 'CREATE VIEW view_name
AS
(query);', '', '', '', '');


INSERT INTO ursql
VALUES ('', '', 'RENAME TABLE view_name TO new_name;', '', 'rename view', '', '');

INSERT INTO ursql
VALUES ('', '', 'DROP VIEW view_name;', '', 'delete views', '', '');

INSERT INTO ursql
VALUES ('', '', 'SHOW FULL TABLES', '', 'list tables & views', '', '');

INSERT INTO ursql
VALUES ('UPDAT', '', '', 'for updatable view, the SELECT statement 
may not contain the following elements
- DISTINCT
- GROUP BY
- HAVING
- UNION
- types of join
- subqueries
', '', '', '');

INSERT INTO ursql
VALUES ('', '', 'UPDATE table_name
SET column1 = value1, column2 = value2....
WHERE (conditions);', '', 'modify records assign new values to columns with 
SET omition of WHERE clause will update all rows
', '', '');

INSERT INTO ursql
VALUES ('DELETE', '', '', '', '', '', '');

INSERT INTO ursql
VALUES ('', '', 'DELETE FROM view_name
WHERE (conditions);', '', 'delete rows omition of WHERE clause will 
delete all rows', 
'', '');

INSERT INTO ursql
VALUES ('INSERT', '', '', '', '', '', '');

INSERT INTO ursql
VALUES ('', 'insert into all columns of a table', 'INSERT INTO table_name
VALUES (value1, value2, value3 ...);', '', '
the order of values must correspond to 
the order of columns in the table', '', '');

exit

sudo mysql -u root -p books -t -e "select * from ursql" > mySQL


